//

import SwiftUI

@main
struct LamflixApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
