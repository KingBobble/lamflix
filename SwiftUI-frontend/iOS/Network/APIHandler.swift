//

import Foundation

final class APIHandler {
    
    func getMovieInfo<T:Decodable>(JSONbody: [String:Any], completion: @escaping (T)->()) {
        do {
            let request = try Endpoint.createURLRequest(url: .movieInfo, method: .POST, JSONbody: JSONbody)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if let data = data {
                    do {
                        let movie = try JSONDecoder().decode(T.self, from: data)
                        
                        //print(movie)
                        DispatchQueue.main.async {
                            completion(movie)
                        }
                    } catch {
                        print(Errors.cantReadURL)
                    }
                }
            }
            task.resume()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getMoviesInfoByGenre<T:Decodable>(JSONbody: [String:Any], completion: @escaping (T)->()) {
        do {
            let request = try Endpoint.createURLRequest(url: .searchByGenre, method: .POST, JSONbody: JSONbody)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if let data = data {
                    do {
                        let movie = try JSONDecoder().decode(T.self, from: data)
                        
                        //print(movie)
                        DispatchQueue.main.async {
                            completion(movie)
                        }
                    } catch {
                        print(Errors.cantReadURL)
                    }
                }
            }
            task.resume()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getMoviesInfoBySearch<T:Decodable>(JSONbody: [String:Any], completion: @escaping (T)->()) {
        do {
            let request = try Endpoint.createURLRequest(url: .searchByMovieName, method: .POST, JSONbody: JSONbody)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if let data = data {
                    do {
                        let movie = try JSONDecoder().decode(T.self, from: data)
                        
                        //print(movie)
                        DispatchQueue.main.async {
                            completion(movie)
                        }
                    } catch {
                        print(Errors.cantReadURL)
                    }
                }
            }
            task.resume()
        } catch {
            print(error.localizedDescription)
        }
    }
}
