//

import Foundation

struct SingleMovieModel: Codable {
    let data: MovieDataModel
    let error: String
    let messages: [String]
    let status: String
}

struct MultipleMoviesModel: Codable {
    let data: ResultsClass
    let error: String
    let messages: [String]
    let status: String
}

struct ResultsClass: Codable {
    let results: [MovieDataModel]
}

struct MovieDataModel: Codable, Identifiable {
    var id = UUID()
    let backdropImage, coverImage: String
    let filmID: Int
    let filmLocation, filmMAC: String
    let genre: [String]
    let name, overview: String

    enum CodingKeys: String, CodingKey {
        case backdropImage = "backdrop_image"
        case coverImage = "cover_image"
        case filmID = "film_id"
        case filmLocation = "film_location"
        case filmMAC = "film_mac"
        case genre, name, overview
    }
}
