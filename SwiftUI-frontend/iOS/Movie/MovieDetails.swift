//

import SwiftUI
import AVKit

struct MovieDetails: View {
    @State var movieDetail: MovieDataModel?
    
    var body: some View {
        ZStack {
            Rectangle()
                .fill(LinearGradient(gradient: Gradient(colors: [.yellow, .black]), startPoint: .top, endPoint: .bottom))
            
            if let movie = movieDetail {
                VStack {
                    ZStack(alignment: .topLeading) {
                        AsyncImage(url: URL(string: Endpoint.coverURL + movie.coverImage))
                        { image in
                            image.resizable()
                        } placeholder: {
                            ZStack {
                                LinearGradient(gradient: Gradient(colors: [.gray, .clear]), startPoint: .top, endPoint: .bottom)
                                ProgressView()
                            }
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        HStack {
                            Spacer()
                            SheetCloseButton()
                        }
                    }
                    
                    ScrollView(showsIndicators: false) {
                        HStack(alignment: .top) {
                            VStack(alignment: .leading) {
                                Text(movie.name)
                                    .font(.system(size: 20, weight: .bold, design: .rounded))
                                
                                Spacer()
                                
                                HStack {
                                    ForEach(movie.genre, id : \.self) { category in
                                        HStack {
                                            Text(category)
                                                .font(.footnote)
                                            if category != movie.genre.last {
                                                Image(systemName: "circle.fill")
                                                    .foregroundColor(.blue)
                                                    .font(.system(size: 3))
                                            }
                                        }
                                    }
                                }
                                
                                Spacer()
                                
                                Text(movie.overview)
                                
                                Spacer()
                                
                                HStack {
                                    if let url = URL(string: Endpoint.videoURL + movie.filmLocation) {
                                        let player = AVPlayer(url: url)
                                        ModeVideoPlayer(player: player)
                                            .frame(minHeight: 216)
                                    }
                                }
                            }
                            .padding()
                        }
                        /*VStack {
                            HStack {
                                if let url = URL(string: Endpoint.videoURL + movie.filmLocation) {
                                    ModeVideoPlayer(player: AVPlayer(url: url))
                                    //VideoPlayer(player: AVPlayer(url: url))
                                        .frame(minHeight: 216)
                                        //.frame(width: 349.1, height: 196.4)
                                }
                            }
                        }*/
                    }
                    Spacer()
                }
            }
        }
    }
}

struct MovieDetails_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetails(movieDetail: nil)
    }
}

struct SheetCloseButton: View {
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View{
        Button {
            presentationMode.wrappedValue.dismiss()
        } label: {
            Image(systemName: "xmark.circle")
                .font(.system(size: 25))
                .foregroundColor(.yellow)
                .padding()
        }
    }
}
