//

import Foundation

class SearchBarState: ObservableObject {
    @Published var movies: MultipleMoviesModel?
    
    public func getSearchResults(text: String) {
        if text.count > 0 {
            let body: [String: Any] = [
                "name_search": text,
                "search_limit": 20
            ]
            
            APIHandler().getMoviesInfoBySearch(JSONbody: body) { (movies) in
                self.movies = movies
            }
        } else {
            self.movies = nil
        }
    }
}
