//

import SwiftUI

struct SearchBarView: View {
    @State private var movieDetailToShow: MovieDataModel?
    @State private var searchText = ""
    @ObservedObject var sbState = SearchBarState()
    @FocusState var searchIsFocused: Bool
    
    var body: some View {
        let searchTextBinding = Binding (
            get: {
                searchText
            }, set: {
                if searchText != $0 {
                    searchText = $0
                    sbState.getSearchResults(text: $0)
                }
            }
        )
        
        VStack {
            SearchBar(searchText: searchTextBinding, searchIsFocused: _searchIsFocused)
            
            Spacer()
            
            if let movie = sbState.movies {
                List(movie.data.results) { (film) in
                    Text(film.name)
                        .onTapGesture {
                            movieDetailToShow = film
                        }
                }
            }
        }
        .onTapGesture {
            if (searchIsFocused != false) {
                searchIsFocused = false
            }
        }
        .sheet(item: $movieDetailToShow) { movie in
            MovieDetails(movieDetail: movie)
        }
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView()
    }
}
