//

import SwiftUI

struct SearchBar: View {
    @Binding var searchText: String
    @FocusState var searchIsFocused: Bool
    
    var body: some View {
        HStack {
            NavigationLink{
                ContentView()
                    .navigationBarHidden(true)
            } label: {
                Image(systemName: "chevron.left")
                    .font(.system(size: 25))
                    .padding()
            }

            ZStack (alignment: .leading) {
                Rectangle()
                    .fill(Color(red: 0.1922, green: 0.1961, blue: 0.2000))
                    .frame(maxWidth: .infinity, maxHeight: 36)
                    .cornerRadius(8)
                
                HStack {
                    Image(systemName: "magnifyingglass")
                        .padding(.leading, 10)
                    
                    TextField("", text: $searchText, prompt: Text("Search for titles...").foregroundColor(.white))
                        .padding(5)
                        .padding(.leading, -5)
                        .background(Color(red: 0.1922, green: 0.1961, blue: 0.2000))
                        .tint(.yellow)
                        .focused($searchIsFocused)
                    
                    Button {
                        //print(self.$searchText)
                        searchText = ""
                        searchIsFocused = false
                    } label : {
                        Image(systemName: "xmark.circle")
                            .foregroundColor(.white)
                    }
                    .padding([.trailing], 10)
                }
            }
        }
        .padding()
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(searchText: .constant(""), searchIsFocused: FocusState())
    }
}
