//

import SwiftUI

struct HomeView: View {
    @State private var movieDetailToShow: MovieDataModel?
    
    var body: some View {
        ZStack {
            ScrollView(showsIndicators: false) {
                TopHeaderBar()
                TopMoviePreview(movieDetail: $movieDetailToShow)
                // Cant have more than 10 views in one view group
                Genres(genreName: Genre.New.rawValue, movieDetail: $movieDetailToShow)
                Genres(genreName: Genre.Action.rawValue, movieDetail: $movieDetailToShow)
                HomeGenres(movieDetailToShow: $movieDetailToShow)
            }
            .sheet(item: $movieDetailToShow) { movie in
                MovieDetails(movieDetail: movie)
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
