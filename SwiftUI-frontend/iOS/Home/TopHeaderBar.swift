//

import SwiftUI

struct TopHeaderBar: View {
    var body: some View {
        HStack {
            Image("default_logo")
                .resizable()
                .scaledToFit()
                .frame(width:50)
                .padding(10)
            
            Spacer()
            
            Button {
                print("FunctionCall")
            } label: {
                Image(systemName: "line.horizontal.3.circle")
                    .font(.system(size: 25))
            }
            .padding([.trailing], 10)
        }
    }
}

struct TopHeaderBar_Previews: PreviewProvider {
    static var previews: some View {
        TopHeaderBar()
    }
}
