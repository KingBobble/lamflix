//

import SwiftUI

struct HomeGenres: View {
    @Binding var movieDetailToShow: MovieDataModel?
    
    var body: some View {
        Genres(genreName: Genre.Adventure.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Comedy.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Thriller.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Horror.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Animation.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Family.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Romance.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Drama.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Crime.rawValue, movieDetail: $movieDetailToShow)
        Genres(genreName: Genre.Science.rawValue, movieDetail: $movieDetailToShow)
    }
}

struct HomeGenres_Previews: PreviewProvider {
    static var previews: some View {
        HomeGenres(movieDetailToShow: .constant(nil))
    }
}
