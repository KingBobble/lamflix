//

import Foundation

class GenresState: ObservableObject {
    @Published var movies: MultipleMoviesModel?
    
    public func getMoviesByGenre(text: String) {
        if text.count > 0 {
            let body: [String: Any] = [
                "genre": text,
            ]
            
            APIHandler().getMoviesInfoByGenre(JSONbody: body) { (movies) in
                self.movies = movies
            }
        } else {
            self.movies = nil
        }
    }
}

enum Genre: String, CaseIterable {
    case New
    case Action
    case Adventure
    case Comedy
    case Thriller
    case Horror
    case Animation
    case Family
    case Romance
    case Drama
    case Crime
    case Science
}
