//

import SwiftUI

struct TopMoviePreview: View {
    @State var topMovie: SingleMovieModel?
    @Binding var movieDetail: MovieDataModel?
    
    var body: some View {
        ZStack {
            if let movie = topMovie {
                AsyncImage(url: URL(string: Endpoint.backdropURL + movie.data.backdropImage))
                { image in
                    image.resizable()
                        //.aspectRatio(contentMode: .fit)
                } placeholder: {
                    ZStack{
                        LinearGradient(gradient: Gradient(colors: [.gray, .clear]), startPoint: .top, endPoint: .bottom)
                        ProgressView()
                    }
                }
                .frame(maxWidth: 480, maxHeight: 267)
                .cornerRadius(8)
                
                Rectangle()
                    .foregroundColor(.clear)
                    .background(LinearGradient(gradient: Gradient(colors: [.clear, .black]), startPoint: .top, endPoint: .bottom))
            }
            
            VStack {
                Spacer()
                
                if let movie = topMovie {
                    Text(movie.data.name)
                        .font(.callout)
                    
                    HStack {
                        ForEach(movie.data.genre, id : \.self) { category in
                            HStack {
                                Text(category)
                                    .font(.footnote)
                                
                                if category != movie.data.genre.last {
                                    Image(systemName: "circle.fill")
                                        .foregroundColor(.blue)
                                        .font(.system(size: 3))
                                }
                            }
                        }
                    }
                }
                
                HStack {
                    Spacer()
                    
                    Button {
                        movieDetail = topMovie?.data
                    } label: {
                        Image(systemName: "play.fill")
                            .font(.system(size: 25))
                            .foregroundColor(.black)
                        Text("Watch Now")
                            .bold()
                            .foregroundColor(.black)
                    }
                    .padding(5)
                    .background(.white)
                    .cornerRadius(8)
                    
                    Spacer()
                }
            }
        }.onAppear {
            let body: [String: Any] = [
                "randomFilm": true
            ]
            APIHandler().getMovieInfo(JSONbody: body) { (movie) in
                self.topMovie = movie
            }
        }
    }
}

struct TopMoviePreview_Previews: PreviewProvider {
    static var previews: some View {
        TopMoviePreview(movieDetail: .constant(nil))
    }
}
