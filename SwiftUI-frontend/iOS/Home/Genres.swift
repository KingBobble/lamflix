//

import SwiftUI

struct Genres: View {
    @ObservedObject var genreState = GenresState()
    @State var genreName: String
    @State var isLoading: Bool = false
    @Binding var movieDetail: MovieDataModel?
    
    var body: some View {
        VStack {
            HStack {
                if genreName == "New" {
                    Text("Recently Added")
                        .bold()
                } else if genreName == "Science" {
                    Text("Science Fiction")
                        .bold()
                } else {
                    Text(genreName)
                        .bold()
                }
                Spacer()
            }.onAppear {
                if !isLoading {
                    isLoading = true
                    genreState.getMoviesByGenre(text: genreName)
                }
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack {
                    if let movie = genreState.movies {
                        ForEach(movie.data.results) { (film) in
                            AsyncImage(url: URL(string: Endpoint.coverURL + film.coverImage))
                            { image in
                                image.resizable()
                            } placeholder: {
                                ZStack{
                                    LinearGradient(gradient: Gradient(colors: [.gray, .clear]), startPoint: .top, endPoint: .bottom)
                                    ProgressView()
                                }
                            }
                            .frame(width: 150, height: 225)
                            .cornerRadius(8)
                            .onTapGesture {
                                movieDetail = film
                            }
                        }
                    }
                }
            }
        }
    }
}

struct Genres_Previews: PreviewProvider {
    static var previews: some View {
        Genres(genreName: "", movieDetail: .constant(nil))
    }
}
