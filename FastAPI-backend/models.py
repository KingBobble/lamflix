from pydantic import BaseModel, validator


class Genre(BaseModel):
    genre: str | None = None

class SingleFilm(BaseModel):
    mac: str | None = None
    randomFilm: bool | None = None

class Search(BaseModel):
    name_search: str | None = None
    search_limit: str | None = 1

class AddFilm(BaseModel):
    mac: str | None = None
    genre: str | None = None
    overview: str | None = None
    name: str | None = None

class Token(BaseModel):
    api_token: str | None = None

class Login(BaseModel):
    username: str
    password: str
    mode: str

    @validator('mode')
    def mode_match(cls, msg):
        if not msg in ['LOGIN', 'REGISTER']:
            raise ValueError("mode must be either set to LOGIN or REGISTER")
        return msg
