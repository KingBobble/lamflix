import uvicorn
import logging
from fastapi import FastAPI, Request, HTTPException
from fastapi.staticfiles import StaticFiles
from config import config_path
import films_database
import json_handler
import models


app = FastAPI(
    docs_url=None,  # Disable docs API
    redoc_url=None  # Disable redoc API
)


@app.post("/api/GetAllFilmInfo")
def get_all_film_info(genre: models.Genre):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    film_response = films_database.get_all_film_info(genre.genre)
    json_response = {"results": []}

    for film in film_response:
        film_json = json_handler.convert_simple_film_to_json(film)
        film_json = json_handler.check_film_sub_folder(film_json)
        json_response["results"].append(film_json)

    returnmessage["data"] = json_response

    return returnmessage


@app.post("/api/GetFilmInfo")
def get_film_info(film: models.SingleFilm):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    film_response = films_database.get_film_info(film.mac, film.randomFilm)

    if film_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_NOT_FOUND"
        returnmessage["messages"].append("DATA NOT FOUND IN DATABASE.")
        raise HTTPException(status_code=400, detail=returnmessage)
    
    film_json = json_handler.convert_simple_film_to_json(film_response)
    film_json = json_handler.check_film_sub_folder(film_json)

    returnmessage["data"] = film_json

    return returnmessage


@app.post("/api/SearchFilmList")
def search_film_list(search: models.Search):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    film_response = films_database.get_search_film_list(search.name_search, search.search_limit)
    json_response = {"results": []}

    for film in film_response:
        film_json = json_handler.convert_simple_film_to_json(film)
        film_json = json_handler.check_film_sub_folder(film_json)
        json_response["results"].append(film_json)

    returnmessage["data"] = json_response

    return returnmessage


@app.post("/api/AddFilm")
def add_film(film: models.AddFilm):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if film.genre is not None:
        film.genre = [x.strip(' ') for x in film.genre]

    status = films_database.add_film(film.name, film.mac, film.genre, film.overview)

    if not status:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data not able to be added to the database.")
        raise HTTPException(status_code=400, detail=returnmessage)

    return returnmessage


@app.post("/api/CheckLoginStatus")
def check_login_status(token: models.Token, request: Request):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    ip_addr = request.client.host

    if ip_addr is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "IP_FAILED"
        returnmessage["messages"].append("IP information is invalid.")
        raise HTTPException(status_code=400, detail=returnmessage)    

    api_response = films_database.check_api_token(ip_addr, token.api_token)

    if api_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        raise HTTPException(status_code=400, detail=returnmessage)

    returnmessage["data"] = api_response

    return returnmessage


@app.post("/api/SendLoginInfo")
def send_login_info(login: models.Login, request: Request):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}
    
    ip_addr = request.client.host

    if ip_addr is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "IP_FAILED"
        returnmessage["messages"].append("IP information is invalid.")
        raise HTTPException(status_code=400, detail=returnmessage)

    api_response = films_database.login_status(login.username, login.password, ip_addr, login.mode)

    if api_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        raise HTTPException(status_code=400, detail=returnmessage)

    returnmessage["data"] = api_response[0]

    return returnmessage


DATAPATH = config_path()
STATIC_PATH = DATAPATH["static_path"]
app.mount("/api", StaticFiles(directory=STATIC_PATH), name="static")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    
    """Run main program with command: uvicorn main:app --host 127.0.0.1 --port 8000 --workers 4

    main: the file main.py (the Python "module").
    app: the object created inside of main.py with the line app = FastAPI().
    --reload: make the server restart after code changes. Only use for development.
    
    Interactive API documentation: http://127.0.0.1:8000/docs

    Gunicorn + Uvicorn combo command:

    gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 127.0.0.1:8000
    """
    
    # localhost
    uvicorn.run("main:app", host="127.0.0.1", port=8000)

    # private ip address
    # uvicorn.run("main:app", host="0.0.0.0", port=8000)#, forwarded_allow_ips='*', reload=True)
