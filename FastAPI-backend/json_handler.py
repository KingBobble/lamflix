import json


def convert_simple_film_to_json(tups):
    jsonvar = {}
    jsonvar["name"] = tups[0]
    jsonvar["film_mac"] = tups[1]
    jsonvar["film_id"] = tups[2]
    jsonvar["genre"] = json.loads(tups[3])
    jsonvar["overview"] = tups[4]
    jsonvar["cover_image"] = tups[5]
    jsonvar["film_location"] = tups[6]
    jsonvar["backdrop_image"] = tups[7]

    return jsonvar


def check_film_sub_folder(film_json):
    film_id = int(film_json["film_id"])
    film_subfolder = None

    if film_id < 51:
        film_subfolder = "1-50/"
    elif film_id >= 51 and film_id < 101:
        film_subfolder = "51-100/"
    elif film_id >= 101 and film_id < 151:
        film_subfolder = "101-150/"
    elif film_id >= 151 and film_id < 201:
        film_subfolder = "151-200/"
    elif film_id >= 201 and film_id < 251:
        film_subfolder = "201-250/"
    elif film_id >= 251 and film_id < 301:
        film_subfolder = "251-300/"
    elif film_id >= 301 and film_id < 351:
        film_subfolder = "301-350/"
    elif film_id >= 351 and film_id < 401:
        film_subfolder = "351-400/"

    if film_subfolder is not None:
        film_json["cover_image"] = film_subfolder + film_json["cover_image"]
        film_json["film_location"] = film_subfolder + film_json["film_location"]
        film_json["backdrop_image"] = film_subfolder + film_json["backdrop_image"]

    return film_json
