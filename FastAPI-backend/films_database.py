import json
import sqlite3
import os
import random
import math
import logging
from time import sleep, time
from datetime import datetime
from config import config_path
import json_handler


DATAPATH = config_path()
DB_PATH = DATAPATH["db_path"]
directory = os.path.dirname(DB_PATH)


def connect_to_database(timeout=5):
    now = time()

    try:
        os.stat(directory)
    except Exception as error:
        # print(error)
        logging.exception("Exception occurred")
        os.mkdir(directory)

    while (time() - now) < timeout:
        try:
            conn = sqlite3.connect(DB_PATH)
            return conn
        except Exception as error:
            # print(error)
            logging.exception("Exception occurred")
            sleep(0.1)
            continue

    return None


def commit_conn(conn, timeout=5):
    if conn is not None:
        now = time()

        while (time() - now) < timeout:
            try:
                conn.commit()
            except Exception as error:
                # print(error)
                logging.exception("Exception occurred")
                sleep(0.1)
                continue
            else:
                break


def create_tables():
    """Create all needed tables, to be ran at startup"""
    create_film_table()
    create_login_table()


def create_film_table():
    """Create the film table if it does not exist, to be ran at start up"""
    conn = connect_to_database()

    if conn is not None:
        curr = conn.cursor()
        curr.execute("CREATE TABLE IF NOT EXISTS 'films' ( `name` TEXT, \
            `film_mac` TEXT UNIQUE COLLATE NOCASE, \
            `film_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
            `genre` json, `overview` TEXT, 'cover_image' TEXT, \
            `film_location` TEXT, `backdrop_image` TEXT,\
            `video_type` TEXT, `connection_status` INTEGER); \
            ")

        commit_conn(conn)
        conn.close()


def create_login_table():
    """Create the login table if it does not exist, to be ran at start up"""
    conn = connect_to_database()

    if conn is not None:
        curr = conn.cursor()
        curr.execute("CREATE TABLE IF NOT EXISTS 'logins' ( `username` TEXT UNIQUE, \
            `password` TEXT, `ip_address` TEXT,\
            `user_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\
            `api_token` json,  `token_timestamp` TEXT, `token_status` INTEGER); \
            ")

        commit_conn(conn)
        conn.close()


def add_film(name=None, film_mac=None, genre=None, overview=None):
    conn = connect_to_database()
    status = False

    if not conn is None:
        curr = conn.cursor()
        exists = []

        try:
            if name is None:
                command = "SELECT * FROM films WHERE film_mac=?;"
                curr.execute(command, (film_mac,))
                exists = curr.fetchone()
            else:
                command = "SELECT * FROM films WHERE name=?;"
                curr.execute(command, (name,))
                exists = curr.fetchone()

            if exists:
                film_info = json_handler.convert_simple_film_to_json(exists)
                film_id = film_info["film_id"]

                if name != film_info["name"]:
                    name = name.title().strip()
                    temp = name.replace(" ", "")
                    film_mac = film_mac_random_generator(temp)
                    cover_image = film_mac + "_c.jpg"
                    film_location = film_mac + ".mp4"
                    backdrop_image = film_mac + "_b.jpg"

                    if genre is None:
                        genre = film_info["genre"]

                    if overview is None:
                        overview = film_info["overview"]

                    command = "UPDATE films SET name=?, film_mac=?, genre=?, overview=?, cover_image=?, film_location=?, backdrop_image=? WHERE film_id=?;"
                    curr.execute(command, (name, film_mac, json.dumps(
                        genre), overview, cover_image, film_location, backdrop_image, film_id,))
                else:
                    if genre is None:
                        genre = film_info["genre"]

                    if overview is None:
                        overview = film_info["overview"]

                    command = "UPDATE films SET genre=?, overview=? WHERE film_id=?;"
                    curr.execute(
                        command, (json.dumps(genre), overview, film_id,))
        except Exception as error:
            # print(error)
            logging.exception("Exception occurred")

        if not exists:
            try:
                name = name.title().strip()
                temp = name.replace(" ", "")
                film_mac = film_mac_random_generator(temp)
                cover_image = film_mac + "_c.jpg"
                film_location = film_mac + ".mp4"
                backdrop_image = film_mac + "_b.jpg"

                if genre is None:
                    genre = []

                command = "INSERT INTO 'films' VALUES (?, ?, NULL, ?, ?, ?, ?, ?, 'movie', 1);"
                curr.execute(command, (name, film_mac, json.dumps(
                    genre), overview, cover_image, film_location, backdrop_image))
            except Exception as error:
                # print(error)
                logging.exception("Exception occurred")

        status = True
        commit_conn(conn)
        conn.close()

    return status


def get_all_film_info(genre=None):
    conn = connect_to_database()
    allfilms = None

    if conn is not None:
        curr = conn.cursor()

        if genre:
            if genre.lower() == "new":
                command = "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE connection_status=1 ORDER BY film_id DESC LIMIT 50;"
            else:
                command = "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE connection_status=1 AND genre LIKE '%" + \
                    genre+"%' ORDER BY RANDOM() LIMIT 50;"

            curr.execute(command)
        else:
            curr.execute(
                "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE connection_status=1;")

        try:
            allfilms = curr.fetchall()
        except Exception as error:
            # print(error)
            logging.exception("Exception occurred")
            allfilms = None

        conn.close()

    return allfilms


def get_film_info(mac=None, random_film=False):
    conn = connect_to_database()
    film = None

    if conn is not None:
        curr = conn.cursor()

        if random_film:
            curr.execute("SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE film_id IN (SELECT film_id FROM 'films' ORDER BY RANDOM() LIMIT 1);")
        elif mac:
            curr.execute(
                "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE film_mac = ? AND connection_status=1;", (mac, ))

        try:
            film = curr.fetchone()
        except Exception as error:
            # print(error)
            logging.exception("Exception occurred")
            film = None

        conn.close()

    return film


def get_search_film_list(name_search=None, search_limit=10):
    conn = connect_to_database()
    allfilms = None

    if conn is not None:
        curr = conn.cursor()

        if name_search:
            command = "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE name LIKE '%"+name_search+"%' LIMIT ?;"
            curr.execute(command, (search_limit,))
        else:
            command = "SELECT name, film_mac, film_id, genre, overview, cover_image, film_location, backdrop_image FROM 'films' WHERE connection_status=1 LIMIT ?;"
            curr.execute(command, (search_limit,))

        try:
            allfilms = curr.fetchall()
        except Exception as error:
            # print(error)
            logging.exception("Exception occurred")
            allfilms = None

        conn.close()

    return allfilms


def film_mac_random_generator(name):
    letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    film_mac = ''
    random.seed(name)

    for _ in range(20):
        film_mac += letters[math.floor(random.random() * 36)]

    return film_mac


def check_api_token(ip_addr, api_token):
    conn = connect_to_database()
    is_token_valid = []
    token_info = None

    if conn is not None:
        curr = conn.cursor()
        command = "SELECT token_timestamp FROM 'logins' WHERE ip_address=? AND api_token=? AND token_status=1 LIMIT 1;"
        curr.execute(command, (ip_addr, api_token,))
        is_token_valid = curr.fetchone()

        if is_token_valid:
            old_timestamp = int(datetime.strftime(datetime.strptime(is_token_valid[0],'%Y-%m-%d %H:%M:%S.%f'), '%d'))
            new_timestamp = int(datetime.strftime(datetime.now(), '%d'))

            if new_timestamp - old_timestamp == 1 or new_timestamp - old_timestamp == 0:
                token_info = api_token

        conn.close()

    return token_info


def login_status(username, password, ip_addr, mode):
    conn = connect_to_database()
    login_info = None

    if conn is not None:
        curr = conn.cursor()

        if mode == "LOGIN":
            is_login_valid = []
            command = "SELECT user_id FROM 'logins' WHERE username=? AND password=? AND ip_address=? LIMIT 1;"
            curr.execute(command, (username, password, ip_addr,))
            is_login_valid = curr.fetchone()

            if is_login_valid:
                api_token = token_random_generator()
                token_timestamp = datetime.now()
                command = "UPDATE logins SET api_token=?, token_timestamp=? WHERE user_id=?;"
                curr.execute(command, (api_token,
                                       token_timestamp, is_login_valid[0]))
                command = "SELECT api_token FROM 'logins' WHERE username=? AND password=? AND ip_address=? LIMIT 1;"
                curr.execute(command, (username, password, ip_addr,))

                try:
                    login_info = curr.fetchone()
                except Exception as error:
                    # print(error)
                    logging.exception("Exception occurred")
                    login_info = None
        elif mode == "REGISTER":
            is_login_valid = []
            command = "SELECT user_id FROM 'logins' WHERE username=? AND password=? AND ip_address=? LIMIT 1;"
            curr.execute(command, (username, password, ip_addr,))
            is_login_valid = curr.fetchone()

            if not is_login_valid:
                api_token = token_random_generator()
                token_timestamp = datetime.now()
                command = "INSERT INTO 'logins' VALUES (?, ?, ?, NULL, ?, ?, 0);"
                curr.execute(command, (username, password,
                                       ip_addr, api_token, token_timestamp,))
                command = "SELECT api_token FROM 'logins' WHERE username=? AND password=? AND ip_address=? LIMIT 1;"
                curr.execute(command, (username, password, ip_addr,))

                try:
                    login_info = curr.fetchone()
                except Exception as error:
                    # print(error)
                    logging.exception("Exception occurred")
                    login_info = None

        commit_conn(conn)
        conn.close()

    return login_info


def token_random_generator():
    letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    token = ''

    for _ in range(32):
        token += letters[math.floor(random.random() * 62)]

    return token
