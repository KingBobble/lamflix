import logging
from flask import Flask, jsonify, request, send_from_directory
import films_database
import json_handler
from config import config_path


DATAPATH = config_path()
STATIC_PATH = DATAPATH["static_path"]
app = Flask(__name__, static_folder=STATIC_PATH, static_url_path='/')


# Testing the adding of movies info to database
# @app.route('/')
# def index():
#    return app.send_static_file('index.html')


@app.route('/api/pictures/covers/<path:filename>', methods=["GET"])
def get_film_cover_images(filename):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    cover_subfolder = filename.split("/")

    if len(cover_subfolder) > 2:
        returnmessage["status"] = "error"
        returnmessage["error"] = "URL_NOT_FOUND"
        returnmessage["messages"].append("URL file is missing or invalid.")
        return jsonify(returnmessage), 400

    cover_location = "/pictures/covers/"

    return send_from_directory(app.static_folder+cover_location, filename)


@app.route('/api/pictures/backdrops/<path:filename>', methods=["GET"])
def get_film_backdrop_images(filename):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    backdrop_subfolder = filename.split("/")

    if len(backdrop_subfolder) > 2:
        returnmessage["status"] = "error"
        returnmessage["error"] = "URL_NOT_FOUND"
        returnmessage["messages"].append("URL file is missing or invalid.")
        return jsonify(returnmessage), 400

    backdrop_location = "/pictures/backdrops/"

    return send_from_directory(app.static_folder+backdrop_location, filename)


@app.route('/api/videos/<path:filename>', methods=["GET"])
def get_film_videos(filename):
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    video_subfolder = filename.split("/")

    if len(video_subfolder) > 2:
        returnmessage["status"] = "error"
        returnmessage["error"] = "URL_NOT_FOUND"
        returnmessage["messages"].append("URL file is missing or invalid.")
        return jsonify(returnmessage), 400

    video_location = "/videos/"

    return send_from_directory(app.static_folder+video_location, filename)


@app.route('/api/GetAllFilmInfo', methods=['GET', 'POST'])
def get_all_film_info():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    content = request.get_json()

    if 'genre' in content:
        genre = content["genre"]
    else:
        genre = None

    json_response = {"results": []}
    film_response = films_database.get_all_film_info(genre)

    for film in film_response:
        film_json = json_handler.convert_simple_film_to_json(film)
        film_json = json_handler.check_film_sub_folder(film_json)
        json_response["results"].append(film_json)

    returnmessage["data"] = json_response

    return jsonify(returnmessage)


@app.route('/api/GetFilmInfo', methods=['GET', 'POST'])
def get_film_info():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    content = request.get_json()

    if 'mac' in content:
        film_mac = content["mac"]
    else:
        film_mac = None

    if 'randomFilm' in content:
        if isinstance(content["randomFilm"], bool):
            random_film = content["randomFilm"]
        else:
            random_film = False
    else:
        random_film = False

    film_response = films_database.get_film_info(film_mac, random_film)

    if film_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_NOT_FOUND"
        returnmessage["messages"].append("DATA NOT FOUND IN DATABASE.")
        return jsonify(returnmessage), 400

    film_json = json_handler.convert_simple_film_to_json(film_response)
    film_json = json_handler.check_film_sub_folder(film_json)

    returnmessage["data"] = film_json

    return jsonify(returnmessage)


@app.route('/api/SearchFilmList', methods=['GET', 'POST'])
def search_film_list():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    content = request.get_json()

    if 'name_search' in content:
        name_search = content["name_search"]
    else:
        name_search = None

    if 'search_limit' in content:
        search_limit = content["search_limit"]
    else:
        search_limit = 1

    json_response = {"results": []}
    film_response = films_database.get_search_film_list(name_search, search_limit)

    for film in film_response:
        film_json = json_handler.convert_simple_film_to_json(film)
        film_json = json_handler.check_film_sub_folder(film_json)
        json_response["results"].append(film_json)

    returnmessage["data"] = json_response

    return jsonify(returnmessage)


@app.route('/api/AddFilm', methods=['GET', 'POST'])
def add_film():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    # get the json content
    content = request.get_json()

    if 'mac' in content:
        mac = content["mac"]
    else:
        mac = None

    if 'genre' in content:
        genre = content["genre"]
        genre = [x.strip(' ') for x in genre]
    else:
        genre = None

    if 'overview' in content:
        overview = content["overview"]
    else:
        overview = None

    if 'name' in content:
        name = content["name"]
    else:
        name = None

    status = films_database.add_film(name, mac, genre, overview)

    if not status:
        returnmessage["status"] = "error"
        returnmessage["error"] = "DATA_ERROR"
        returnmessage["messages"].append("Data not able to be added to the database.")
        return jsonify(returnmessage), 400

    return jsonify(returnmessage)


@app.route('/api/CheckLoginStatus', methods=['GET','POST'])
def check_login_status():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    content = request.get_json()

    if 'api_token' in content:
        api_token = content["api_token"]
    else:
        api_token = None

    ip_addr = request.environ["HTTP_X_FORWARDED_FOR"]

    api_response = films_database.check_api_token(ip_addr, api_token)

    if api_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        return jsonify(returnmessage), 400

    returnmessage["data"] = api_response

    return jsonify(returnmessage)


@app.route('/api/SendLoginInfo', methods=['POST'])
def send_login_info():
    returnmessage = {"status": "success",
                     "error": "", "messages": [], "data": {}}

    if not request.is_json:
        returnmessage["status"] = "error"
        returnmessage["error"] = "FORMAT_ERROR"
        returnmessage["messages"].append("JSON format is missing or invalid.")
        return jsonify(returnmessage), 400

    content = request.get_json()

    if 'username' in content:
        username = content["username"]
    else:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        return jsonify(returnmessage), 400

    if 'password' in content:
        password = content["password"]
    else:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        return jsonify(returnmessage), 400

    if 'mode' in content:
        mode = content["mode"]
    else:
        mode = None

    ip_addr = request.environ["HTTP_X_FORWARDED_FOR"]

    api_response = films_database.login_status(username, password, ip_addr, mode)

    if api_response is None:
        returnmessage["status"] = "error"
        returnmessage["error"] = "LOGIN_FAILED"
        returnmessage["messages"].append("Login information is invalid or expired.")
        return jsonify(returnmessage), 400

    returnmessage["data"] = api_response[0]

    return jsonify(returnmessage)


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    films_database.create_tables()

    # default production mode
    app.run()

    # private ipv4 address, Debug: False for non-auto updating files on save else True
    # app.run(host='0.0.0.0', port=5000, debug=False)

    # localhost, Debug: False for non-auto updating files on save else True
    # app.run(host='127.0.0.1', port=5000, debug=False)
