import os
import films_database
import json_handler
from config import config_media


def name_swap():
    os_mode = False # True for Windows, False for Linux
    video_mode = False # True for video, False for picture
    image_mode = False # True for covers, False for backdrops

    MEDIA = config_media()
    VIDEO_PATH = MEDIA["video_path"]
    COVER_PATH = MEDIA["cover_path"]
    BACKDROP_PATH = MEDIA["backdrop_path"]

    if video_mode:
        video_folder = VIDEO_PATH
        folder = video_folder
    else:
        if image_mode:
            picture_folder = COVER_PATH
        else:
            picture_folder = BACKDROP_PATH
        folder = picture_folder

    film_response = films_database.get_all_film_info()

    for film in film_response:
        dev_json = json_handler.convert_simple_film_to_json(film)

        for _, filename in enumerate(os.listdir(folder)):
            if video_mode:
                film_id = filename.replace(".mp4", "")
            else:
                film_id = filename.replace(".jpg", "")

            if str(dev_json["film_id"]) == film_id:
                if os_mode:
                    src = folder + filename
                else:
                    src = folder + "\\" + filename

                if video_mode:
                    if os_mode:
                        dst = folder + "\\" +dev_json["film_location"] 
                    else:
                        dst = folder + dev_json["film_location"]
                else:
                    if image_mode:
                        if os_mode:
                            dst = folder + "\\" + dev_json["cover_image"]
                        else:
                            dst = folder + dev_json["cover_image"]
                    else:
                        if os_mode:
                            dst = folder + "\\" + dev_json["backdrop_image"] 
                        else:
                            dst = folder + dev_json["backdrop_image"] 

                os.rename(src, dst)


if __name__ == '__main__':
    name_swap()
