from configparser import ConfigParser


def config_path(filename='config.ini', section='datapath'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    path = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            path[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return path


def config_media(filename='config.ini', section='media'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    path = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            path[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return path
