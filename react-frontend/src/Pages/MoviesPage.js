import React from "react";
import Row from "../Components/Row/Row";
import Banner from "../Components/Banner/Banner";

function MoviesPage() {
    return (
        <div className="moviesPage">
            <Banner />
            <Row
                title="LAMFLIX ORIGINALS"
                isLargeRow={true} />
            <Row
                title="Recently Added"
                genreType="New" />    
            <Row
                title="Action"
                genreType="Action" />
            <Row
                title="Adventure"
                genreType="Adventure" />
            <Row
                title="Comedy"
                genreType="Comedy" />
            <Row
                title="Thriller"
                genreType="Thriller" />
            <Row
                title="Horror"
                genreType="Horror" />
            <Row
                title="Animation"
                genreType="Animation" />
            <Row
                title="Family"
                genreType="Family" />
            <Row
                title="Romance"
                genreType="Romance" />
            <Row
                title="Drama"
                genreType="Drama" />
            <Row
                title="Crime"
                genreType="Crime" />
            <Row
                title="Science Fiction"
                genreType="Science Fiction" />
        </div>
    );
}

export default MoviesPage;