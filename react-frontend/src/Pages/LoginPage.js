import React, { useState, useEffect, useRef } from "react";
import { useHistory } from 'react-router-dom';
import { Button, Form } from "react-bootstrap";
function LoginPage() {
  const [loginInfo, setLoginInfo] = useState([]);
  const username = useRef(null);
  const password = useRef(null);
  const history = useHistory();

  useEffect(() => {
    if (loginInfo.length) {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "username": loginInfo[0], "password": loginInfo[1], "mode": loginInfo[2] })
      };
      fetch('/api/SendLoginInfo', requestOptions)
        .then(response => response.json())
        .then(json => {
          if (json.status !== "error") {
            sessionStorage.setItem("api_token", json.data);
            history.push(`/main`);
          } else {
            history.push(`/login`);
          }
        })
        .catch(error => {
          console.log(error);
          history.push(`/login`);
        });
    }

  }, [loginInfo]);

  const LoginHandler = (e) => {
    setLoginInfo([username.current.value, password.current.value, e.currentTarget.value])
  };
  return (
    <div className="LoginPage">
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control ref={username} type="email" placeholder="Enter email" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control ref={password} type="password" placeholder="Password" />
        </Form.Group>
        <Button variant="primary" value={"LOGIN"} onClick={LoginHandler}>
          Login
        </Button>
        <Button variant="primary" value={"REGISTER"} onClick={LoginHandler}>
          Register
        </Button>
      </Form>
    </div>
  );
}

export default LoginPage;