import React, { useState, useEffect } from "react";
import "./MovieItem.scss";
import { useLocation } from 'react-router-dom'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faChevronLeft
} from "@fortawesome/free-solid-svg-icons";
import { useHistory } from 'react-router-dom';

const backdrop_url = process.env.PUBLIC_URL + "/api/pictures/backdrops/";
const cover_url = process.env.PUBLIC_URL + "/api/pictures/covers/";
const video_url = process.env.PUBLIC_URL + "/api/videos/";

function MovieItem() {
    const [movie, setMovie] = useState([]);
    const location = useLocation();
    const history = useHistory();

    try {
        var [mac] = useState(location.state.mac);
    } catch {
    }
    const handleClick = () => {
        history.push(`/movies`);
    };
    useEffect(() => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ "mac": mac })
        };
        fetch('/api/GetFilmInfo', requestOptions)
            .then(response => response.json())
            .then(json => {
                if (json.status !== "error"){
                    setMovie(json.data);
                } else {
                    history.push(`/movies`);
                }});
    }, [mac,history]);


    return (
        <div className="movieItem">
            <div className="item-page">
                {movie.backdrop_image && <img
                    src={backdrop_url + movie.backdrop_image}
                    alt=""
                    className="item-page__bg"
                />}
                <div className="item">
                    <FontAwesomeIcon
                        icon={faChevronLeft}
                        className="row__left-arrow"
                        onClick={() => handleClick()}
                    />
                    <div className="item__outer">
                        <div className="item__inner">
                            <div className="item__img-box">
                                {movie.cover_image && <img src={cover_url + movie.cover_image} alt="poster" className="item__poster-img" />}
                            </div>
                            <div className="item__text-box">
                                <h1 className="item__title">{movie.name}</h1>
                                <span className="item__overview">{movie.overview}</span>
                                <div className="item__vid-box">
                                    {movie.film_location &&
                                        <video width="90%" height="90%" controls >
                                            <source src={video_url + movie.film_location} type="video/mp4" />
                                        </video>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default MovieItem;