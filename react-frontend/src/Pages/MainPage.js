import React, { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom';

function MainPage() {
  const [token, setToken] = useState(sessionStorage.api_token);
  const history = useHistory();

  useEffect(() => {
    if (token) {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "api_token": token })
      };
      fetch('/api/CheckLoginStatus', requestOptions)
        .then(response => response.json())
        .then(json => {
          if (json.status !== "error") {
            setToken(json.data);
            history.push(`/movies`);
          } else {
            history.push(`/login`);
          }
        })
        .catch(error => {
          console.log(error)
          history.push(`/login`);
        });
    } else {
      history.push(`/login`);
    }

  }, []);

  return (
    <div className="MainPage">
    </div>
  );
}

export default MainPage;