import React, { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'
import Row from "../Components/Row/Row";
import Banner from "../Components/Banner/Banner";

function SearchPage() {
    const location = useLocation();
    const searchName = useState(location.state);
    const [searchList, setSearchList] = useState([]);

    useEffect(() => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "name_search": searchName.searchTitle,
                "search_limit": 30
            })
        };
        fetch('/api/SearchFilmList', requestOptions)
            .then(response => response.json())
            .then(json => {
                if (json.status !== "error") {
                    setSearchList(json.data.results);
                }
            });
    }, [searchName.searchTitle]);

    return (
        <div className="SearchPage">
            <Banner />
            <Row
                title="Search Results"
                isLargeRow={true}
                movieList={searchList}
                genreType="Search"
            />
        </div>
    );
}

export default SearchPage;