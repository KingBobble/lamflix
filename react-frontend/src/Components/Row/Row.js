import React, { useState, useEffect } from "react";
import "./Row.css";
/*import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight
} from "@fortawesome/free-solid-svg-icons";*/
import { useHistory } from 'react-router-dom';
import {
  BrowserView,
  MobileView
} from "react-device-detect";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const cover_url = process.env.PUBLIC_URL + "/api/pictures/covers/";

function Row({ title, isLargeRow, genreType, movieList }) {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    if (genreType) {
      if (genreType === "Search") {
        setMovies(movieList);
      } else {
        const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ "genre": genreType })
        };
        fetch('/api/GetAllFilmInfo', requestOptions)
          .then(response => response.json())
          .then(json => setMovies(json.data.results));
      }
    } else {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({})
      };
      fetch('/api/GetAllFilmInfo', requestOptions)
        .then(response => response.json())
        .then(json => setMovies(json.data.results));

    }
  }, [movieList]);

  const history = useHistory();
  const handleClick = (movie) => {
    history.push(`/movies/${movie.film_mac}`, { mac: movie.film_mac });
  };

  return (
    <div className="row">
      <h2 className="row__title">{title}</h2>
      <BrowserView>
        <div className="row__posters">
          {/*<FontAwesomeIcon
              icon={faChevronLeft}
              className="row__left-arrow"
            />*/}
          {movies.map((movie) => (
            <LazyLoadImage
              effect="blur"
              placeholderSrc={cover_url + movie.cover_image}
              key={movie.film_mac}
              onClick={() => handleClick(movie)}
              className={`row__poster ${isLargeRow && "row__posterLarge"}`}
              src={cover_url + movie.cover_image}
              alt={movie.name}
            />
          ))}
        </div>
      </BrowserView>
      <MobileView>
        <div className="row__posters">
          {movies.map((movie) => (
            <LazyLoadImage
              effect="blur"
              placeholderSrc={cover_url + movie.cover_image}
              key={movie.film_mac}
              onClick={() => handleClick(movie)}
              className={`row__poster ${isLargeRow && "row__posterLarge"}`}
              src={cover_url + movie.cover_image}
              alt={movie.name}
            />
          ))}
        </div>
      </MobileView>
    </div>
  );
}

export default Row;
