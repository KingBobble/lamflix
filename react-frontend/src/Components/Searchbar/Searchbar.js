import React, { useState, useEffect } from "react";
import "./Searchbar.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faSearch
} from "@fortawesome/free-solid-svg-icons";
import { useHistory } from 'react-router-dom';

function Searchbar() {
    const [searchTitle, setSearchTitle] = useState("");
    const history = useHistory();

    useEffect(() => {
        if (searchTitle) {
            if (searchTitle.length > 0) {
                history.push("/search", { searchTitle });
            }
        } else {
            history.push("/movies");
        }
    }, [searchTitle]);

return (
    <div className="searchbar">
        <span >
            <FontAwesomeIcon
                icon={faSearch}
                className="searchbar__search-icon"
            />
        </span>
        <input
            className="searchbar__input"
            type="search"
            id="search"
            value={searchTitle}
            onChange={(event) => setSearchTitle(event.target.value)}
            placeholder="     Search"
        />
    </div >
);
}

export default Searchbar;
