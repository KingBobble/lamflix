import React, { useState, useEffect } from "react";
import "./Nav.css";
import Searchbar from "../Searchbar/Searchbar";

const logo_url = process.env.PUBLIC_URL + "/assets/defaultlogo.png";
const profile_url = process.env.PUBLIC_URL + "/assets/defaultprofile.png";

function Nav() {
  const [show, handleShow] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100) {
        handleShow(true);
      } else handleShow(false);
    });
    return () => {
      window.removeEventListener("scroll");
    };
  }, []);

  return (
    <div className={`nav ${show && "nav__black"}`}>
      <img
        className="nav__logo"
        src={logo_url}
        alt="Lamflix Logo"
      />
      <Searchbar />
      <img
        className="nav__avatar"
        src={profile_url}
        alt="Lamflix Profile"
      />
    </div>
  );
}

export default Nav;
