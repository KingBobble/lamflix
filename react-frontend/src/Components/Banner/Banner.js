import React, { useState, useEffect } from "react";
import "./Banner.css";
import { useHistory } from 'react-router-dom';

const backdrop_url = process.env.PUBLIC_URL + "/api/pictures/backdrops/";

function Banner() {
  const [movie, setMovie] = useState([]);
  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ "randomFilm": true })
    };
    fetch('/api/GetFilmInfo', requestOptions)
      .then(response => response.json())
      .then(json => setMovie(json.data));
  }, []);

  function truncate(str, n) {
    return str?.length > n ? str.substr(0, n - 1) + "..." : str;
  }

  const history = useHistory();
  const handleClick = () => {
    history.push(`/movies/${movie.film_mac}`, { mac: movie.film_mac });
  }

  return (
    <header
      className="banner"
      style={{
        backgroundSize: "cover",
        backgroundImage: movie.backdrop_image && `url(${backdrop_url + movie?.backdrop_image})`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner__contents">
        <h1 className="banner__title">
          {movie?.name}
        </h1>

        <div className="banner__buttons">
          <button onClick={handleClick} className="banner__button">Play</button>
          <button onClick={handleClick} className="banner__button">More Info</button>
        </div>

        <h1 className="banner__description">
          {truncate(movie?.overview, 150)}
        </h1>
      </div>

      <div className="banner--fadeBottom" />
    </header>
  );
}

export default Banner;
