import React from 'react';
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import MovieItem from './Pages/MovieItem';
import LoginPage from './Pages/LoginPage';
import SearchPage from './Pages/SearchPage';
import MainPage from './Pages/MainPage';
import MoviesPage from "./Pages/MoviesPage"
import Nav from "./Components/Nav/Nav";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/main">
            <MainPage />
          </Route>
          <Route exact path="/movies">
            <Nav />
            <MoviesPage />
          </Route>
          <Route exact path="/movies/:title">
            <Nav />
            <MovieItem />
          </Route>
          <Route exact path="/search">
            <Nav />
            <SearchPage />
          </Route>
          <Route exact path="/login">
            <LoginPage />
          </Route>
          <Redirect to="/main">
          </Redirect>
        </Switch>
      </div>
    </Router >
  );
}

export default App;